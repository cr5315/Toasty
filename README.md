# Toasty
---

Toasty is a Telegram bot for Poptart's Toaster. This is the code that makes it work.

If you want to run your own Toasty for whatever reason, you'll need to set up your config.py.

```python
ADMINS = [
    1234567  # Use the /whoami command to get your Telegram user ID
]
TELEGRAM_TOKEN = "TELEGRAM HTTP API KEY"
QUOTES_LOCATION = "/location/to/quotes.json"
```

To run the bot, execute python run.py

### Contributing

Fork, code, pull request

#### License
TBD, probably MIT