certifi==2017.4.17
future==0.16.0
python-telegram-bot==5.3.1
urllib3==1.20
wheel==0.24.0
