# -*- coding: utf-8 -*-
from config import ADMINS
from functools import wraps


def command(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        print("[{}] from_user[{}], text[{}]".format(
            update.message.date.strftime("%Y-%m-%d %H:%M:%S"),
            update.message.from_user.id,
            update.message.text
        ))
        return func(bot, update, *args, **kwargs)

    return wrapped


def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        try:
            user_id = update.message.from_user.id
        except (NameError, AttributeError):
            try:
                user_id = update.inline_query.from_user.id
            except (NameError, AttributeError):
                try:
                    user_id = update.chosen_inline_result.from_user.id
                except (NameError, AttributeError):
                    try:
                        user_id = update.callback_query.from_user.id
                    except (NameError, AttributeError):
                        print("No user_id available in update")
                        return

        if user_id not in ADMINS:
            print("Unauthorized access denied.")
            return

        return func(bot, update, *args, **kwargs)

    return wrapped
