# -*- coding: utf-8 -*-
from config import TELEGRAM_TOKEN
from telegram.ext import CommandHandler, Updater
from quotes import cmd_quote, cmd_add_quote, cmd_delete_quote
from util import command, restricted
import logging
import os
import time
import sys


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
updater = Updater(TELEGRAM_TOKEN)


@command
def cmd_help(bot, update):
    help_message = "Hi, I'm Toasty! Here's what I can do:\n" \
                   "/addQuote - Reply to a message to add it to my quotes\n" \
                   "/quote - Get a random quote\n" \
                   "/quote # - Get a specific quote (Coming soon)\n" \
                   "If you have feature ideas, feel free to share!"

    return update.message.reply_text(help_message)


@command
@restricted
def cmd_restart(bot, update):
    update.message.reply_text("Restarting...")
    time.sleep(0.2)
    os.execl(sys.executable, sys.executable, *sys.argv)
    return


@command
def cmd_start(bot, update):
    return update.message.reply_text("Hello, world!")


@command
@restricted
def cmd_stop(bot, update):
    update.message.reply_text("Good night.")
    updater.stop()
    sys.exit()  # This one doesn't get a return because it would never be called


@command
def cmd_whoami(bot, update):
    return update.message.reply_text("You are {}".format(update.message.from_user.id))


def start_bot():
    print("Toasty, warming up")

    quotes.init()

    updater.dispatcher.add_handler(CommandHandler("addQuote", cmd_add_quote))
    updater.dispatcher.add_handler(CommandHandler("deleteQuote", cmd_delete_quote))
    updater.dispatcher.add_handler(CommandHandler("help", cmd_help))
    updater.dispatcher.add_handler(CommandHandler("quote", cmd_quote))
    updater.dispatcher.add_handler(CommandHandler("restart", cmd_restart))
    updater.dispatcher.add_handler(CommandHandler("start", cmd_start))
    updater.dispatcher.add_handler(CommandHandler("stop", cmd_stop))
    updater.dispatcher.add_handler(CommandHandler("whoami", cmd_whoami))

    updater.start_polling()
    updater.idle()

    return
