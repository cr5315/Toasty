# -*- coding: utf-8 -*-
from config import QUOTES_LOCATION
from util import command, restricted
import json
import os.path
import random

QUOTES = None


def init():
    global QUOTES

    if not os.path.isfile(QUOTES_LOCATION):
        print("Unable to open quotes.json from {}, creating blank quotes file.".format(QUOTES_LOCATION))
        with open(QUOTES_LOCATION, "w") as f:
            f.write("{}")
            QUOTES = {}
    else:
        with open(QUOTES_LOCATION, "r") as f:
            QUOTES = json.load(f)

    return


@command
def cmd_add_quote(bot, update):
    global QUOTES

    if QUOTES is None:
        return update.message.reply_text("I can't find my quotes. This is awkward.")

    message = update.message.reply_to_message

    if message is None:
        return update.message.reply_text("Please reply to the message you would like to add to quote.")

    new_quote = {
        "added_by": {
            "id": update.message.from_user.id,
            "first_name": update.message.from_user.first_name,
            "last_name": update.message.from_user.last_name,
            "username": update.message.from_user.username
        },
        "date": update.message.date.strftime("%Y-%m-%d %H:%M:%S"),
        "text": message.text,
        "username": message.from_user.username,
        "user_id": message.from_user.id,
        "user_first_name": message.from_user.first_name,
        "user_last_name": message.from_user.last_name
    }

    QUOTES[str(len(QUOTES) + 1)] = new_quote
    _save()

    return update.message.reply_text("Quote added!")


@command
@restricted
def cmd_delete_quote(bot, update):
    if QUOTES is None:
        return update.message.reply_text("I can't find my quotes. This is awkward.")


@command
def cmd_quote(bot, update):
    if QUOTES is None:
        return update.message.reply_text("I can't find my quotes. This is awkward.")

    q = _get_quote()
    f_name = q[0].get("user_first_name")
    l_name = q[0].get("user_last_name")
    text = q[0].get("text")

    name = f_name
    if l_name is not None and l_name != u"":
        name += " " + l_name

    return update.message.reply_text("[{} of {}] {}: {}".format(q[1], q[2], name, text))


# noinspection PyTypeChecker
def _get_quote(which=-1):
    if QUOTES is None:
        return None

    if len(QUOTES) == 1:
        which = 1

    if which == -1:
        which = random.randrange(1, len(QUOTES))

    return QUOTES[str(which)], which, len(QUOTES)


def _save():
    with open(QUOTES_LOCATION, "w") as f:
        json.dump(QUOTES, f)

    return
